<?php
/**
 * Dieses Template Part wird für die About-Section genutzt
 * Voraussetzungen:
 * 	- Es gibt eine Seite, die als Slug "about" definiert hat
 *  - Dieses File heisst content-about.php
 * 	- Wir rufen im Loop get_template_part('template-parts/content', 'about') auf
 */
?>
<h1>ABOUT TEMPLATE PART</h1>

		<?php the_title(); ?>
		<?php the_content(); ?>

