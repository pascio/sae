<?php
/**
 * Dieses Template Part wird als Fallback genutzt
 */
?>
<h1>DEFAULT TEMPLATE PART</h1>

<?php the_title(); ?>
<?php
    $images = get_post_gallery_images( $post );
    if( count($images) > 0 ) :
?>
    <div class="slider">
    <?php foreach( $images as $image ) : ?>
    <div class="slide">
        <img src="<?php echo $image; ?>"
    </div>
    <?php endforeach; ?>
    </div>
    <?php endif; ?>
<?php the_content(); ?>


