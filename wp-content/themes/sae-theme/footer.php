<?php
/**
 * The template for displaying the footer
 */
?>
<?php wp_nav_menu(array('theme_location' => 'social-media')); ?>
<?php wp_footer(); ?>
</body>
</html>