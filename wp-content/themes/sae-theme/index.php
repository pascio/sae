<?php
/**
 * The main template file
 */

get_header();

if(have_posts()) {

    while(have_posts()) {

        // retrieves the next post and sets up the post
        the_post();

        // Display post content, zb.
        the_title();
        the_content();
    }

}

get_footer();