<?php
/**
 * The template for displaying the header
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <?php
        $meta_description = get_field('meta_description');
        if($meta_description === "") {
            $meta_description = get_bloginfo('description');
        }
    ?>
    <meta name="description" content="<?php echo $meta_description; ?>" />
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
DÖNER<?php the_excerpt(); ?>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-23586997-1', 'auto');
    ga('send', 'pageview');

</script>

    <img src="<?php header_image(); ?>" alt="" />

<?php wp_nav_menu('main');