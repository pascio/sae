<?php
/*
    Template Name: Single Page Template 
*/


get_header();

        /**
         * Wir machen eine neue WP_Query, um Posts (in diesem Fall alle Pages) aus der Datenbank zu holen
         * Beachte: 'posts_per_page' = wieviele Posts sollen geladen werden - default kommt aus den Wordpress-Einstellungen
         */
        $the_query = new WP_Query(array(
            'post_type' => 'page',
            'posts_per_page' => -1, // wieviele Posts sollen geladen werden? '-1' = alle
            'orderby' => 'menu_order',
            'order' => 'DESC'
        ));

        if( $the_query->have_posts()) {

            while($the_query->have_posts()) {

                /**
                 * the_post() definiert die $post-Variable global..
                 */
                $the_query->the_post();

                /** ..darum kann man hier darauf zugreifen
                 * Wir möchten, dass die Page als Section auf unserem Onepager erscheint - die ID benötigen wir später für die Anchor-Links
                 * Beachte: 'post_name' = Slug, der letzte Teil des Permalinks (editierbar direkt unter dem Titel einer Page im Backend)
                 */
                echo "<section id='$post->post_name' class='page'>";

                    /** Nun möchten wir, dass die Section mithilfe eines bestimmten Template-Parts dargestellt wird.
                     * Dazu nutzen wir wieder den Slug der Seite - hierfür muss für jede Seite/Slug ein File mit Namen "content-{$slug}.php vorhanden sein.
                     * z.B. für die Page: http://example.com/about =>  "template-parts/content-about.php"
                     */
                    if( !is_home() ) { // Nur wenn wir nicht auf der statischen Startseite sind
                        get_template_part("template-parts/content", $post->post_name); // entspricht also für die Page http://example.com/about folgendem: get_template_part("template-parts/content", "about");
                        the_post_thumbnail('thumbnail'); // gib das Featured Image dieser Seite aus
                    }


                echo "<hr>";
                echo "</section>";


            }

        }

        /**
         * Da weiter oben die globale $post Variable verändert wurde, müssen wir sie hier wieder zurücksetzen
         */
        wp_reset_postdata();

get_sidebar();
get_footer();

