<?php
/*
Die Funktion „wp_enqueue_style“ fügt als <style>-Element das CSS-File des Parent-Themes im <head> hinzu. Dadurch werden die CSS-Definitionen des Parent-Themes übernommen.  Der Wert „parent-style“ ist die Bezeichnung der CSS-Datei und wird als ID ins <style>-Element geschrieben.
*/

add_action('wp_enqueue_scripts', 'theme_enqueue_styles');

function theme_enqueue_styles() {

    wp_enqueue_style(
        'styles',
        get_stylesheet_uri(),
        false,
        filemtime(get_stylesheet_directory() . "/style.css")
    );

}

// Navigations-Position "Main Menu" registrieren
register_nav_menus(array(
    'main' => 'Main Menu',
    'social-media' => 'Social Media'
));
//register_nav_menu('social-media', 'Social Media');

// Post Thumbnails für Posts und Pages aktivieren
add_theme_support( 'post-thumbnails' );
add_theme_support( 'excerpt' );

function add_excerpt_for_pages() {
    add_post_type_support( 'page', 'excerpt' );
}

add_action( 'init', 'add_excerpt_for_pages' );

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page();

}



// Header aktivieren
add_theme_support( 'custom-header' );

function lad_mini_styles() {
    add_editor_style( 'editor_style.css' );
}
add_action( 'admin_init', 'lad_mini_styles' );

function load_google_font() {
    $font_url = str_replace( ',', '%2C', 'https://fonts.googleapis.com/css?family=Revalia' );
    add_editor_style( $font_url );
}
add_action( 'admin_init', 'load_google_font' );

remove_filter('the_content', 'wpautop');
