<?php
/*
Die Funktion „wp_enqueue_style“ fügt als <style>-Element das CSS-File des Parent-Themes im <head> hinzu. Dadurch werden die CSS-Definitionen des Parent-Themes übernommen.  Der Wert „parent-style“ ist die Bezeichnung der CSS-Datei und wird als ID ins <style>-Element geschrieben.
*/

add_action('wp_enqueue_scripts', 'theme_enqueue_styles');

function theme_enqueue_styles() {

    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}
