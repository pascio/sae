<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sae');

/** MySQL database username */
define('DB_USER', 'sae');

/** MySQL database password */
define('DB_PASSWORD', 'sae');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'NOI.mW])>8vS?II28e&|W%GsPa>o)j`V:qB<=q!#{c{<m3C}`>jzCl;EEE@@%jhh');
define('SECURE_AUTH_KEY',  '9PQ+QO@I(GB`c7{|`*|*]M>LzABZH,gG2YF`IhRRn2-6pbX4m74Nc%En`6roueic');
define('LOGGED_IN_KEY',    'e!.=(m_KiN9~O|VG/tkwJn6Gv84TE[=xMN{v)O}Na~LVsTVe6R hJF:IU</=iS}3');
define('NONCE_KEY',        '45)jukv 1^|=cTWWyi%rVl8fH4i}jKM81gA9}a(*U]5t+N{t-;Dq*s~x|@=HQ%RQ');
define('AUTH_SALT',        'sm_A)&wz-04ZtbW{6y/:;,WJJ9}p Z.Bti4%m5:,~e|PC^!EbVcM+w(&U|&0vXw0');
define('SECURE_AUTH_SALT', 'X{%/{PkI-H.=IlCv-/{^ua$Q&Ym .Iea1Sk2NiEbk}0m6Ja Ul:qvf(TgDw_X,/,');
define('LOGGED_IN_SALT',   '@av41cMc4{:9`VID_~>aQ<}XhmY}y]]6q/+7g/4tI4.YKvo4<Gow[Sb,$L2lCLq[');
define('NONCE_SALT',       'j{}WM61 ~P_PbLy4C/58(4!N;uhu eUNMmlhXR-y%cM9Bl#/or+v.Ab*EI<eZ)Uv');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
define('WP_ALLOW_REPAIR', true);